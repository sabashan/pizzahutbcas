package ap.dp.builder.common;

/**
 * 
 * @author sabashan
 *
 */
public interface Item {

	public String name();

	public String size();

	public float price();

}
