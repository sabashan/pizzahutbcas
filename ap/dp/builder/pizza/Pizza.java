package ap.dp.builder.pizza;

import ap.dp.builder.common.Item;

/**
 * 
 * @author sabashan
 *
 */
public abstract class Pizza implements Item  {   
    
        @Override
        public abstract float price();

    
}
