package ap.dp.builder.drink;

import ap.dp.builder.common.Item;

/**
 * 
 * @author sabashan
 *
 */
public abstract  class ColdDrink implements Item{

    @Override
    public abstract float price();
    
}
